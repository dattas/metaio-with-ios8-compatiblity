//
//  ViewController.m
//  MyARSample
//
//  Created by vairat on 26/06/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
    NSLog(@"Loading view..............");
    
   [self loadModels];
    [self loadTrackingConfigurationFile];
    
   
}





-(void)loadModels{
    
    NSLog(@"loadModels..............");
    // NSString *metaiomanResourepath = [[NSBundle mainBundle] pathForResource:@"metaioman" ofType:@"md2" inDirectory:@"Assets"];
    
    NSString *metaiomanResourepath =  [[NSBundle mainBundle] pathForResource:@"car" ofType:@"zip"];
    NSString *sailboatResourepath = [[NSBundle mainBundle] pathForResource:@"camera" ofType:@"zip"];
    NSString *elephantResourepath = [[NSBundle mainBundle] pathForResource:@"factory" ofType:@"zip"];
    NSString *boyResourepath = [[NSBundle mainBundle] pathForResource:@"plasticChair" ofType:@"zip"];
    NSString *gentlemanResourepath = [[NSBundle mainBundle] pathForResource:@"officeChair" ofType:@"zip"];
    
    if(metaiomanResourepath)
    {
        NSLog(@"metaiomanResourepath finded");
        _metaiomanModel = m_metaioSDK->createGeometry([metaiomanResourepath UTF8String]);
        
        if(_metaiomanModel)
        {
            _metaiomanModel->setScale(metaio::Vector3d(75.0,75.0,75.0));
            _metaiomanModel->setVisible(true);
        }
        else{
            NSLog(@"error could not load");
        }
    }
    
//    else{
//        NSLog(@"could not find _metaiomanModel path");
//    }
    
    if(sailboatResourepath)
    {
        NSLog(@"sailboatResourepath finded");
        _sailboatModel = m_metaioSDK->createGeometry([sailboatResourepath UTF8String]);
        
        if(_sailboatModel)
        {
            _sailboatModel->setScale(metaio::Vector3d(130.0,130.0,130.0));
            _sailboatModel->setVisible(true);
        }
        else{
            NSLog(@"error could not load");
        }
    }
    
//    else{
//        NSLog(@"could not find  _sailboatModel path");
//    }
    
    if(elephantResourepath)
    {
        NSLog(@"elephantResourepath finded");
        _elephantModel = m_metaioSDK->createGeometry([elephantResourepath UTF8String]);
        
        if(_elephantModel)
        {
            _elephantModel->setScale(metaio::Vector3d(53.0,53.0,53.0));
            _elephantModel->setVisible(true);
        }
        else{
            NSLog(@"error could not load");
        }
    }
    
//    else{
//        NSLog(@"could not find  _elephantModel path");
//    }
    
    if(boyResourepath)
    {
        NSLog(@"boyResourepath finded");
        _boyModel = m_metaioSDK->createGeometry([boyResourepath UTF8String]);
        
        if(_boyModel)
        {
            _boyModel->setScale(metaio::Vector3d(2.0,2.0,2.0));
            _boyModel->setVisible(true);
        }
        else{
            NSLog(@"error could not load");
        }
    }
    
    if(gentlemanResourepath)
    {
        NSLog(@"gentlemanResourepath finded");
        _gentlemanModel = m_metaioSDK->createGeometry([gentlemanResourepath UTF8String]);
        
        if(_gentlemanModel)
        {
            _gentlemanModel->setScale(metaio::Vector3d(160.0,160.0,160.0));
            _gentlemanModel->setVisible(true);
        }
        else{
            NSLog(@"error could not load");
        }
    }

    
       
    
    
}
-(void)loadTrackingConfigurationFile{
    
    NSLog(@"loadTrackingConfigurationFile..............");
    
   // NSString *idMarkerTrackingFile = [[NSBundle mainBundle] pathForResource:@"TrackingData_Marker" ofType:@"xml" inDirectory:@"Assets"];
    
    NSString *idMarkerTrackingFile = [[NSBundle mainBundle] pathForResource:@"TrackingData_Marker" ofType:@"xml"];
    
    if(idMarkerTrackingFile)
    {
        NSLog(@"idMarkerTrackingFile finded..............");
        bool success = m_metaioSDK->setTrackingConfiguration([idMarkerTrackingFile UTF8String]);
        if(!success)
            NSLog(@"Failure....");
        else
        {
            
            int cosID = m_metaioSDK->getCoordinateSystemID("COS1");
            NSLog(@"cosID::%d",cosID);
            
            int cosID2 = m_metaioSDK->getCoordinateSystemID("COS2");
            NSLog(@"cosID2::%d",cosID2);
            
            int cosID3 = m_metaioSDK->getCoordinateSystemID("COS3");
            NSLog(@"cosID2::%d",cosID3);
            
            int cosID4 = m_metaioSDK->getCoordinateSystemID("COS4");
            NSLog(@"cosID2::%d",cosID4);
            
            int cosID5 = m_metaioSDK->getCoordinateSystemID("COS5");
            NSLog(@"cosID2::%d",cosID5);
            
            _metaiomanModel->setCoordinateSystemID(cosID);
            _sailboatModel->setCoordinateSystemID(cosID2);
            _elephantModel->setCoordinateSystemID(cosID3);
            _boyModel->setCoordinateSystemID(cosID4);
            _gentlemanModel->setCoordinateSystemID(cosID5);
            
        }
        
    }
    else{
        NSLog(@"idMarkerTrackingFile could not find path");
    }
    
    
    
    
    
    
}


-(void)setActivityModal:(int)index{
    
    switch (index) {
        case 0:
            _metaiomanModel->setVisible(true);
            _sailboatModel->setVisible(false);
            break;
            
        case 1:
            _metaiomanModel->setVisible(false);
            _sailboatModel->setVisible(true);
            break;
            
        default:
            break;
    }
    
    
    
}


- (IBAction)onModalChanges:(UISegmentedControl *)sender {

    
    [self setActivityModal:[sender selectedSegmentIndex]];
}

- (IBAction)photoButton_Clicked:(id)sender {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    
   
           
       // [imagePicker setSourceType:glView];
            
    [imagePicker setDelegate:self];
    //[self presentViewController:imagePicker animated:YES completion:NULL];

}




-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info

{
    NSLog(@"imagePickerController");
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    
    
  //  [self.imageView setImage:image];



}




- (void)drawFrame
{
    [super drawFrame];
    
    // return if the metaio SDK has not been initialiyed yet
    if( !m_metaioSDK )
        return;
   /*
    // get all the detected poses/targets
    std::vector<metaio::TrackingValues> poses = m_metaioSDK->getTrackingValues();
    
    //if we have detected one, attach our metaioman to this coordinate system ID
    if(poses.size())
        _metaiomanModel->setCoordinateSystemID( poses[0].coordinateSystemID );   
    
    
    int cosID = m_metaioSDK->getCoordinateSystemID("COS1");
    NSLog(@"cosID::%d",cosID);
    
    */
    
   // NSLog(@"poses[0].coordinateSystemID::%d",poses[0].coordinateSystemID);
    // _sailboatModel->setCoordinateSystemID( poses[1].coordinateSystemID );
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
