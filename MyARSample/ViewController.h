//
//  ViewController.h
//  MyARSample
//
//  Created by vairat on 26/06/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MetaioSDKViewController.h"
@interface ViewController : MetaioSDKViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>



@property(nonatomic)metaio::IGeometry *metaiomanModel;
@property(nonatomic)metaio::IGeometry *sailboatModel;
@property(nonatomic)metaio::IGeometry *elephantModel;
@property(nonatomic)metaio::IGeometry *boyModel;
@property(nonatomic)metaio::IGeometry *gentlemanModel;

-(void)loadModels;
-(void)loadTrackingConfigurationFile;
-(void)setActivityModal:(int)index;

- (IBAction)onModalChanges:(UISegmentedControl *)sender;
- (IBAction)photoButton_Clicked:(id)sender;
@end
